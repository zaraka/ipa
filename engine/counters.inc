initCounter1:
  mov eax, [surfNum0]
  mov DWORD [counter1 + object.surface], eax
  
  mov [counter1 + object.data], byte 0

  mov DWORD [counter1 + object.funcUpdate], updateCounter1 
  
  mov DWORD [counter1 + object.funcDraw], drawCounter1 
  ret

drawCounter1:
  call SDL_UpperBlit, [counter1 + object.surface], 0, [hScreen], counter1 + object.pos
  SDL_Error  
  ret
  
initCounter2:
  mov eax, [surfNum0]
  mov DWORD [counter2 + object.surface], eax
  
  mov [counter2 + object.data], byte 0
  
  mov DWORD [counter2 + object.funcUpdate], updateCounter2
  
  mov DWORD [counter2 + object.funcDraw], drawCounter2 
  ret
  
drawCounter2:
  call SDL_UpperBlit, [counter2 + object.surface], 0, [hScreen], counter2 + object.pos
  SDL_Error  
  ret
  

updateCounter1:
  cmp [counter1 + object.data], byte 0
  jne updateCounter1.num1
  mov eax, [surfNum0]
  mov [counter1 + object.surface], eax
  jmp updateCounter1.end
.num1:

  cmp [counter1 + object.data], byte 1
  jne updateCounter1.num2
  mov eax, [surfNum1]
  mov [counter1 + object.surface], eax
  jmp updateCounter1.end
.num2:

  cmp [counter1 + object.data], byte 2
  jne updateCounter1.num3
  mov eax, [surfNum2]
  mov [counter1 + object.surface], eax
  jmp updateCounter1.end
.num3:

  cmp [counter1 + object.data], byte 3
  jne updateCounter1.num4
  mov eax, [surfNum3]
  mov [counter1 + object.surface], eax
  jmp updateCounter1.end
.num4:

  cmp [counter1 + object.data], byte 4
  jne updateCounter1.num5
  mov eax, [surfNum4]
  mov [counter1 + object.surface], eax
  jmp updateCounter1.end
.num5:

    cmp [counter1 + object.data], byte 5
  jne updateCounter1.num6
  mov eax, [surfNum5]
  mov [counter1 + object.surface], eax
  jmp updateCounter1.end
.num6:

  cmp [counter1 + object.data], byte 6
  jne updateCounter1.num7
  mov eax, [surfNum6]
  mov [counter1 + object.surface], eax
  jmp updateCounter1.end
.num7:

  cmp [counter1 + object.data], byte 7
  jne updateCounter1.num8
  mov eax, [surfNum7]
  mov [counter1 + object.surface], eax
  jmp updateCounter1.end
.num8:

  cmp [counter1 + object.data], byte 8
  jne updateCounter1.num9
  mov eax, [surfNum8]
  mov [counter1 + object.surface], eax
  jmp updateCounter1.end
.num9:
  mov eax, [surfNum9]
  mov [counter1 + object.surface], eax
.end:  
  ret

updateCounter2:
  cmp [counter2 + object.data], byte 0
  jne updateCounter2.num1
  mov eax, [surfNum0]
  mov [counter2 + object.surface], eax
  jmp updateCounter2.end
.num1:

  cmp [counter2 + object.data], byte 1
  jne updateCounter2.num2
  mov eax, [surfNum1]
  mov [counter2 + object.surface], eax
  jmp updateCounter2.end
.num2:

  cmp [counter2 + object.data], byte 2
  jne updateCounter2.num3
  mov eax, [surfNum2]
  mov [counter2 + object.surface], eax
  jmp updateCounter2.end
.num3:

  cmp [counter2 + object.data], byte 3
  jne updateCounter2.num4
  mov eax, [surfNum3]
  mov [counter2 + object.surface], eax
  jmp updateCounter2.end
.num4:

  cmp [counter2 + object.data], byte 4
  jne updateCounter2.num5
  mov eax, [surfNum4]
  mov [counter2 + object.surface], eax
  jmp updateCounter2.end
.num5:

    cmp [counter2 + object.data], byte 5
  jne updateCounter2.num6
  mov eax, [surfNum5]
  mov [counter2 + object.surface], eax
  jmp updateCounter2.end
.num6:

  cmp [counter2 + object.data], byte 6
  jne updateCounter2.num7
  mov eax, [surfNum6]
  mov [counter2 + object.surface], eax
  jmp updateCounter2.end
.num7:

  cmp [counter2 + object.data], byte 7
  jne updateCounter2.num8
  mov eax, [surfNum7]
  mov [counter2 + object.surface], eax
  jmp updateCounter2.end
.num8:

  cmp [counter2 + object.data], byte 8
  jne updateCounter2.num9
  mov eax, [surfNum8]
  mov [counter2 + object.surface], eax
  jmp updateCounter2.end
.num9:
  mov eax, [surfNum9]
  mov [counter2 + object.surface], eax
.end:  
  ret