initBall:
  SDL_LoadBMP imgball
  mov DWORD [ball + object.surface], eax  
  
  mov DWORD [ball + object.funcUpdate], updateBall
  
  mov DWORD [ball + object.funcDestroy], destroyBall
  
  mov DWORD [ball + object.funcDraw], drawBall
  ret
    
drawBall:
  call SDL_UpperBlit, [ball + object.surface], 0, [hScreen], ball + object.pos
  SDL_Error
  ret
    
destroyBall:
  call SDL_FreeSurface, [ball + object.surface]
  ret  
  
updateBall:

  call collisionDetection

  mov eax, [ball + object.pos + SDL_Rect.x]
  add eax, [ball + object.velX]
  mov [ball + object.pos + SDL_Rect.x], eax
  
  mov eax, [ball + object.pos + SDL_Rect.y]
  add eax, [ball + object.velY]
  mov [ball + object.pos + SDL_Rect.y], eax

  ret
  
collisionDetection:
  mov bx, [ball + object.pos + SDL_Rect.x]
  add bx, 16
  cmp bx, [rocket2 + object.pos + SDL_Rect.x]
  jb _xIsOnrocket2
  mov bx, [rocket2 + object.pos + SDL_Rect.y]
  sub bx, 16
  cmp bx, [ball + object.pos + SDL_Rect.y]
  ja _xIsOnrocket2
  mov bx, [rocket2 + object.pos + SDL_Rect.y]
  add bx, 48
  cmp bx, [ball + object.pos + SDL_Rect.y]
  jb _xIsOnrocket2
  
  mov ecx, 0
  sub ecx, [ball + object.velX]
  mov [ball + object.velX], ecx
  
  mov bx, [rocket2 + object.pos + SDL_Rect.y]
  add bx, 16
  cmp bx, [ball + object.pos + SDL_Rect.y]
  ja _lessRocket2
  mov [ball + object.velY], DWORD 1
  jmp _xIsOnrocket2
_lessRocket2:
  mov [ball + object.velY], DWORD -1
_xIsOnrocket2:
  
  mov bx, [ball + object.pos + SDL_Rect.x]
  sub bx, 16
  cmp bx, [rocket1 + object.pos + SDL_Rect.x]
  ja _xIsOnrocket1
  mov bx, [rocket1 + object.pos + SDL_Rect.y]
  sub bx, 16
  cmp bx, [ball + object.pos + SDL_Rect.y]
  ja _xIsOnrocket1
  mov bx, [rocket1 + object.pos + SDL_Rect.y]
  add bx, 48
  cmp bx, [ball + object.pos + SDL_Rect.y]
  jb _xIsOnrocket1  
  
  mov ecx, 0
  sub ecx, [ball + object.velX]
  mov [ball + object.velX], ecx
  
  mov bx, [rocket1 + object.pos + SDL_Rect.y]
  add bx, 16
  cmp bx, [ball + object.pos + SDL_Rect.y]
  ja _lessRocket1
  mov [ball + object.velY], DWORD 1
  jmp _xIsOnrocket1
_lessRocket1:
  mov [ball + object.velY], DWORD -1
_xIsOnrocket1: 

  mov bx, [ball + object.pos + SDL_Rect.y]
  cmp bx, 464
  jb _ballNoHitDown
  mov ebx, 0
  sub ebx, [ball + object.velY]
  mov [ball + object.velY], ebx
  mov edx, 0
  cmp edx, [ball + object.velX]
  jg _ballNoHitDownF
  add [ball + object.velX], DWORD 1
  jmp _ballNoHitTop
_ballNoHitDownF: 
  sub [ball + object.velX], DWORD 1  
_ballNoHitDown: 

  mov bx, [ball + object.pos + SDL_Rect.y]
  cmp bx, 0
  ja _ballNoHitTop
  mov ebx, 0
  sub ebx, [ball + object.velY]
  mov [ball + object.velY], ebx
  mov edx, 0
  cmp edx, [ball + object.velX]
  jl _ballNoHitTopF
  add [ball + object.velX], DWORD 1
  jmp _ballNoHitTop
_ballNoHitTopF: 
  sub [ball + object.velX], DWORD 1
_ballNoHitTop: 

  mov bx, [ball + object.pos + SDL_Rect.x]
  cmp bx, 0
  ja _ballNoHitLeft
  
  add [counter2 + object.data], byte 1
  cmp [counter2 + object.data], byte 10
  jne _noReset2
  call printf, victoryPlayer2
  mov [counter1 + object.data], byte 0
  mov [counter2 + object.data], byte 0
_noReset2:
  
  mov [ball + object.pos + SDL_Rect.x], word 312
  mov [ball + object.pos + SDL_Rect.y], word 216
  mov [ball + object.pos + SDL_Rect.w], word 16
  mov [ball + object.pos + SDL_Rect.h], word 16  
  mov [ball + object.velX], dword 3
  mov [ball + object.velY], dword -1  
  
_ballNoHitLeft: 

  mov bx, [ball + object.pos + SDL_Rect.x]
  cmp bx, 640
  jb _ballNoHitRight
  
  add [counter1 + object.data], byte 1
  cmp [counter1 + object.data], byte 10
  jne _noReset1
  call printf, victoryPlayer1
  mov [counter1 + object.data], byte 0
  mov [counter2 + object.data], byte 0
_noReset1:

  mov [ball + object.pos + SDL_Rect.x], word 312
  mov [ball + object.pos + SDL_Rect.y], word 216
  mov [ball + object.pos + SDL_Rect.w], word 16
  mov [ball + object.pos + SDL_Rect.h], word 16  
  mov [ball + object.velX], dword -3
  mov [ball + object.velY], dword -1  
  
_ballNoHitRight: 

  ret