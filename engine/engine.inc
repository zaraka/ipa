%ifndef ENGINE_INC
%define ENGINE_INC

;ENUM_OBJECT_TYPE
  OBJECT_UNKNOWN equ 0
  OBJECT_ROCKET equ 1
  OBJECT_BALL equ 2
  OBJECT_COUNTER equ 3

  ;to store all game objects
  struc object
    .pos: resb SDL_Rect_size
    .velX: resd 1 
    .velY: resd 1
    .funcDraw: resd 1 ;void*
    .funcUpdate: resd 1 ;void*
    .funcInit: resd 1 ;void* this function should be called first!
    .funcDestroy: resd 1 ;void* this function should be called last
    .type: resb 1 ;byte enum
    .surface: resd 1 ;SDL_Surface*
    .data: resb 1 ;void*
  endstruc

%include 'engine/rockets.inc'
%include 'engine/ball.inc'
%include 'engine/counters.inc'
  
initGlobals:
  SDL_LoadBMP number0
  mov [surfNum0], eax
  SDL_LoadBMP number1
  mov DWORD [surfNum1], eax
  SDL_LoadBMP number2
  mov DWORD [surfNum2], eax
  SDL_LoadBMP number3
  mov DWORD [surfNum3], eax
  SDL_LoadBMP number4
  mov DWORD [surfNum4], eax
  SDL_LoadBMP number5
  mov DWORD [surfNum5], eax
  SDL_LoadBMP number6
  mov DWORD [surfNum6], eax
  SDL_LoadBMP number7
  mov DWORD [surfNum7], eax
  SDL_LoadBMP number8
  mov DWORD [surfNum8], eax
  SDL_LoadBMP number9
  mov DWORD [surfNum9], eax
  ret
  
destroyGlobals:
  call SDL_FreeSurface, [surfNum0]
  mov [surfNum0], dword 0
  call SDL_FreeSurface, [surfNum1]
  mov [surfNum1], dword 0
  call SDL_FreeSurface, [surfNum2]
  mov [surfNum2], dword 0
  call SDL_FreeSurface, [surfNum3]
  mov [surfNum3], dword 0
  call SDL_FreeSurface, [surfNum4]
  mov [surfNum4], dword 0
  call SDL_FreeSurface, [surfNum5]
  mov [surfNum5], dword 0
  call SDL_FreeSurface, [surfNum6]
  mov [surfNum6], dword 0
  call SDL_FreeSurface, [surfNum7]
  mov [surfNum7], dword 0
  call SDL_FreeSurface, [surfNum8]
  mov [surfNum8], dword 0
  call SDL_FreeSurface, [surfNum9]
  mov [surfNum9], dword 0
  ret
  

  
checkKey:
  call SDL_PollEvent, event
  cmp eax,byte 0
  je _action
  
  cmp byte [event], byte SDL_QUIT
  je _quitLoop
  
  ;posouvani
  cmp word [event + SDL_Event.type], word SDL_KEYDOWN
  jne _NoKeyDown
  
  cmp word [event + SDL_Event.key + SDL_KeyboardEvent.keysym + SDL_keysym.sym], word SDLK_DOWN
  jne _presedNextUp
  mov [rocket2 + object.velY], dword 4 

_presedNextUp:  
  cmp word [event + SDL_Event.key + SDL_KeyboardEvent.keysym + SDL_keysym.sym], word SDLK_UP
  jne _presedNextDown
  mov [rocket2 + object.velY], dword -4 

_presedNextDown:  
  cmp word [event + SDL_Event.key + SDL_KeyboardEvent.keysym + SDL_keysym.sym], word SDLK_s
  jne _presedNextW 
  mov [rocket1 + object.velY], dword 4 

_presedNextW:  
  cmp word [event + SDL_Event.key + SDL_KeyboardEvent.keysym + SDL_keysym.sym], word SDLK_w
  jne _NoKeyDown
  mov [rocket1 + object.velY], dword -4 
  
  jmp _action
_NoKeyDown:  
cmp word [event + SDL_Event.type], word SDL_KEYUP
  jne _action  
  
  cmp word [event + SDL_Event.key + SDL_KeyboardEvent.keysym + SDL_keysym.sym], word SDLK_DOWN
  jne _pullNextUp
  mov [rocket2 + object.velY], dword 0 
  
_pullNextUp:
  cmp word [event + SDL_Event.key + SDL_KeyboardEvent.keysym + SDL_keysym.sym], word SDLK_UP
  jne _pullNextS
  mov [rocket2 + object.velY], dword 0 
  
_pullNextS:
  cmp word [event + SDL_Event.key + SDL_KeyboardEvent.keysym + SDL_keysym.sym], word SDLK_s
  jne _pullNextW
  mov [rocket1 + object.velY], dword 0

_pullNextW:  
  cmp word [event + SDL_Event.key + SDL_KeyboardEvent.keysym + SDL_keysym.sym], word SDLK_w
  jne _action
  mov [rocket1 + object.velY], dword 0
  
_action:
  
  jmp _endCheckUpdate
  
%endif