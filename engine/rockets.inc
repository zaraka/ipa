initRocket1:	;initRocket()
  SDL_LoadBMP imgrocket
  mov DWORD [rocket1 + object.surface], eax
   
  mov DWORD [rocket1 + object.funcUpdate], updateRocket1
  
  mov DWORD [rocket1 + object.funcDestroy], destroyRocket1
  
  mov DWORD [rocket1 + object.funcDraw], drawRocket1
  ret  
  
drawRocket1:
  call SDL_UpperBlit, [rocket1 + object.surface], 0, [hScreen], rocket1 + object.pos
  SDL_Error
  ret
  
initRocket2:	;initRocket()
  SDL_LoadBMP imgrocket
  mov DWORD [rocket2 + object.surface], eax
  
  mov DWORD [rocket2 + object.funcUpdate], updateRocket2
  
  mov DWORD [rocket2 + object.funcDestroy], destroyRocket2
  
  mov DWORD [rocket2 + object.funcDraw], drawRocket2
  ret

drawRocket2:
  call SDL_UpperBlit, [rocket2 + object.surface], 0, [hScreen], rocket2 + object.pos
  SDL_Error
  ret
  
destroyRocket1:
  call SDL_FreeSurface, [rocket1 + object.surface]
  ret
  
destroyRocket2:
  call SDL_FreeSurface, [rocket2 + object.surface]
  ret  
  
  
updateRocket1:
  mov eax, [rocket1 + object.pos + SDL_Rect.x]
  add eax, [rocket1 + object.velX]
  mov [rocket1 + object.pos + SDL_Rect.x], eax
  
  mov eax, [rocket1 + object.pos + SDL_Rect.y]
  add eax, [rocket1 + object.velY]
  mov [rocket1 + object.pos + SDL_Rect.y], eax
  cmp [rocket1 + object.pos + SDL_Rect.y], word 432
  jb updateRocket1.endUpdateRocket1
  mov [rocket1 + object.pos + SDL_Rect.y], word 432
.endUpdateRocket1:
  cmp [rocket1 + object.pos + SDL_Rect.y], word 5
  ja updateRocket1.endUpdateRocket1Next
  mov [rocket1 + object.pos + SDL_Rect.y], word 5

.endUpdateRocket1Next:  
  ret

updateRocket2:
  mov eax, [rocket2 + object.pos + SDL_Rect.x]
  add eax, [rocket2 + object.velX]
  mov [rocket2 + object.pos + SDL_Rect.x], eax

  mov eax, [rocket2 + object.pos + SDL_Rect.y]
  add eax, [rocket2 + object.velY]
  mov [rocket2 + object.pos + SDL_Rect.y], eax
  cmp [rocket2 + object.pos + SDL_Rect.y], word 432
  jb updateRocket2.endUpdateRocket2
  mov [rocket2 + object.pos + SDL_Rect.y], word 432
.endUpdateRocket2:
  cmp [rocket2 + object.pos + SDL_Rect.y], word 5
  ja updateRocket2.endUpdateRocket2Next
  mov [rocket2 + object.pos + SDL_Rect.y], word 5
.endUpdateRocket2Next:  
  
  ret