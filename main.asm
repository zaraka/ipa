bits 32

;include libraries
%include 'lib/SDL.inc'
%include 'engine/engine.inc'

  global main ;main function
  
[SECTION .data]
 string windowTitle, "ASM SDL"
 ;LOGs
 string log, "SDL in NASM", 10
 string quitText, "Exiting", 10
 ;Images
 string imgrocket, "img/rocket.bmp"
 string imgball, "img/ball.bmp"
 ;Number Load strings
 string number0, "img/zero.bmp"
 string number1, "img/one.bmp"
 string number2, "img/two.bmp"
 string number3, "img/three.bmp"
 string number4, "img/four.bmp"
 string number5, "img/five.bmp"
 string number6, "img/six.bmp"
 string number7, "img/seven.bmp"
 string number8, "img/eight.bmp"
 string number9, "img/nine.bmp"
 string victoryPlayer1, "Player 1 has win the match!", 10
 string victoryPlayer2, "Player 2 has win the match!", 10
  
  string intString, " %d ", 10
 
 ;This is not pretty, consider making it more pretty
  surfNum0: dd 0
  surfNum1: dd 0
  surfNum2: dd 0
  surfNum3: dd 0
  surfNum4: dd 0
  surfNum5: dd 0
  surfNum6: dd 0
  surfNum7: dd 0 
  surfNum8: dd 0
  surfNum9: dd 0

counter1:
  istruc object
    at object.pos + SDL_Rect.x, dw 50
    at object.pos + SDL_Rect.y, dw 0
    at object.pos + SDL_Rect.w, dw 20
    at object.pos + SDL_Rect.h, dw 30
    at object.velX, dd 0
    at object.velY, dd 0
    at object.type, db OBJECT_COUNTER
    at object.data, db 0
  iend
  
counter2:
  istruc object
    at object.pos + SDL_Rect.x, dw 550
    at object.pos + SDL_Rect.y, dw 0
    at object.pos + SDL_Rect.w, dw 20
    at object.pos + SDL_Rect.h, dw 30
    at object.velX, dd 0
    at object.velY, dd 0
    at object.type, db OBJECT_COUNTER
    at object.data, db 0
  iend
 
rocket1:
  istruc object
    at object.pos + SDL_Rect.x, dw 10
    at object.pos + SDL_Rect.y, dw 200
    at object.pos + SDL_Rect.w, dw 16
    at object.pos + SDL_Rect.h, dw 48
    at object.velX, dd 0
    at object.velY, dd 0
    at object.type, db OBJECT_ROCKET
  iend

rocket2:
  istruc object
    at object.pos + SDL_Rect.x, dw 614
    at object.pos + SDL_Rect.y, dw 200
    at object.pos + SDL_Rect.w, dw 16
    at object.pos + SDL_Rect.h, dw 48
    at object.velX, dd 0
    at object.velY, dd 0
    at object.type, db OBJECT_ROCKET
  iend 
  
ball:
  istruc object
    at object.pos + SDL_Rect.x, dw 312
    at object.pos + SDL_Rect.y, dw 216
    at object.pos + SDL_Rect.w, dw 16
    at object.pos + SDL_Rect.h, dw 16
    at object.velX, dd 3
    at object.velY, dd -1
    at object.type, db OBJECT_BALL
  iend 
   
event:
  istruc SDL_Event
    at SDL_Event.type, db 0
    at SDL_Event.active, db 0
    at SDL_Event.key, db 0
    at SDL_Event.motion, db 0
    at SDL_Event.button, db 0
    at SDL_Event.jaxis, db 0
    at SDL_Event.jball, db 0
    at SDL_Event.jhat, db 0
    at SDL_Event.jbutton, db 0
    at SDL_Event.resize, db 0
    at SDL_Event.expose, db 0
    at SDL_Event.quit, db 0
    at SDL_Event.user, db 0
    at SDL_Event.sysqm, db 0
  iend
 
 velocity dd 0
 angle dd 0
 touchY dd 0
 constant2 dd 2
 constant16 dd 16
 
 
 
[SECTION .bss]
 hScreen resd 1 
[SECTION .text]
main:
  call printf, log
  
  call SDL_Init, SDL_INIT_VIDEO
  SDL_Error
 
  call SDL_SetVideoMode, 640, 480, 8, SDL_DOUBLEBUF
  mov [hScreen], eax
  SDL_Error_NULL 
 
  call SDL_WM_SetCaption, windowTitle, 0
  call SDL_FillRect, [hScreen], 0, 1000
    
   mov DWORD [rocket1 + object.funcInit], initRocket1
   mov DWORD [rocket2 + object.funcInit], initRocket2
   mov DWORD [ball + object.funcInit], initBall
   mov DWORD [counter1 + object.funcInit], initCounter1
   mov DWORD [counter2 + object.funcInit], initCounter2
   
   
   call initGlobals
   call [rocket1 + object.funcInit]
   call [rocket2 + object.funcInit]
   call [ball + object.funcInit]
   call [counter1 + object.funcInit]
   call [counter2 + object.funcInit]
   
  ;jmp _quitLoop
_mainLoop:
  jmp checkKey
_endCheckUpdate:



_update:
;UPDATE LOGIC
  call [rocket1 + object.funcUpdate]
  call [rocket2 + object.funcUpdate]
  call [ball + object.funcUpdate]
  call [counter1 + object.funcUpdate]
  call [counter2 + object.funcUpdate]
  
;RENDERING
  call SDL_FillRect, [hScreen], 0, 0 ;draw black first
  call [ball + object.funcDraw]
  call [rocket1 + object.funcDraw]
  call [rocket2 + object.funcDraw]
  ;counter will be at top
  call [counter1 + object.funcDraw]
  call [counter2 + object.funcDraw]
    
  
  
  call SDL_Delay, 16
  call SDL_Flip, [hScreen],0,0,0,0
  ;call SDL_UpdateRect, [hScreen],0,0,0,0

  jmp _mainLoop
_quitLoop:  
  ;----------------DESTROY OBJECT-------------
  call [rocket1 + object.funcDestroy]
  call [rocket2 + object.funcDestroy]
  call [ball + object.funcDestroy]
  call destroyGlobals
  
  call SDL_Quit
  
  call printf, quitText
  
  ret ;main

