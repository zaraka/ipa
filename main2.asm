bits 32

;include libraries
%include 'lib/SDL.inc'
  
  global main
  
[SECTION .data]
 string initError, "SDL", 10
 string windowTitle, "ASM SDL"
 ;LOGs
 string log, "SDL in NASM", 10, "v1.0.0", 10
 ;SDL config
 string renderDriver, "SDL_RENDER_DRIVER"
 string driver, "software"
 string quitText, "Exiting", 10
 
screenRect:
 istruc SDL_Rect
  at SDL_Rect.x, dw 0
  at SDL_Rect.y, dw 0
  at SDL_Rect.w, dw 10
  at SDL_Rect.h, dw 10
 iend
 
event:
  istruc SDL_Event
    at SDL_Event.type, db 0
    at SDL_Event.active, db 0
    at SDL_Event.key, db 0
    at SDL_Event.motion, db 0
    at SDL_Event.button, db 0
    at SDL_Event.jaxis, db 0
    at SDL_Event.jball, db 0
    at SDL_Event.jhat, db 0
    at SDL_Event.jbutton, db 0
    at SDL_Event.resize, db 0
    at SDL_Event.expose, db 0
    at SDL_Event.quit, db 0
    at SDL_Event.user, db 0
    at SDL_Event.sysqm, db 0
  iend
 
 
[SECTION .bss]
 hScreen resd 1 
[SECTION .text]
main:
  call printf, log

  call SDL_Init, SDL_INIT_VIDEO
  SDL_Error
  
  call SDL_SetVideoMode, 640, 480, 8, 0
  mov [hScreen], eax
  SDL_Error_NULL
  
  call SDL_WM_SetCaption, windowTitle, 0
  
  call SDL_FillRect, [hScreen], screenRect, 1000
  
  ;jmp _quitLoop
_mainLoop:
  call SDL_PollEvent, event
  cmp eax,byte 0
  je _noEvents
  
  cmp byte [event], byte SDL_QUIT
  je _quitLoop
  
_noEvents:
  call SDL_UpdateRect, [hScreen],0,0,0,0
  

  jmp _mainLoop
_quitLoop:
  call printf, quitText
  
  call SDL_Quit
  
  ret ;main
  