CC=gcc
CFLAGS=-m32 
CLIBS=-lc -lSDL
NASM=nasm
NFLAGS=-g -f elf32
BIN=sdl

all: clean sdl

sdl: clean main.o
	$(CC) $(CFLAGS) main.o -o $(BIN) $(CLIBS)

main.o:
	$(NASM) $(NFLAGS) main.asm
clean:
	rm -rf *.o
	
#