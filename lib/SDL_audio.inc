%ifndef SDL_AUDIO_INC
%define SDL_AUDIO_INC

;---------------------------AUDIO-----------------------------

  SDL_MIX_MAXVOLUME equ 128

;---- Functions

  extern SDL_AudioInit
  extern SDL_AudioQuit
  
  extern SDL_AudioDriverName
  extern SDL_OpenAudio
  extern SDL_GetAudioStatus
  extern SDL_PauseAudio
  
  extern SDL_LoadWAV_RW
  extern SDL_FreeWav
  
  extern SDL_BuildAudioCVT
  extern SDL_ConvertAudio
  extern SDL_MixAudio
  
  extern SDL_LockAudio
  extern SDL_UnlockAudio
  extern SDL_CloseAudio
  
  struc SDL_AudioSpec
    freq: resd 1 ;int
    format: resw 1 ;uint16
    channels: resb 1 ;uint8
    silence: resb 1 ;uint8
    samples: resw 1 ;uint16
    padding: resw 1 ;uint16
    size: resd 1 ;uint32
  endstruc
  
  struc SDL_AudioCVT
    needed: resd 1 ;int
    src_format: resw 1 ;uint16
    dst_format: resw 1 ;uint16
    rate_incr: resd 1 ;double
    buf: resd 1 ;uint8*
    len: resd 1 ;int
    len_cvt: resd 1 ;int
    len_mult: resd 1 ;int
    len_ratio: resd 1 ;double
    filters: resd 1 ;void (SDLCALL *filters[10])(struct SDL_AudioCVT *cvt, Uint16 format);
    filter_index: resd 1;int
  endstruc
  
  
%endif