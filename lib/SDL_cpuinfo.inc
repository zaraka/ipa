%ifndef SDL_CPUINFO_INC
%define SDL_CPUINFO_INC

  ;SDL_cpuinfo.h
  
  extern SDL_HasRDTSC
  extern SDL_HasMMX
  extern SDL_HasMMXExt
  extern SDL_Has3DNow
  extern SDL_Has3DNowExt
  extern SDL_HasSSE
  extern SDL_HasSSE2
  extern SDL_HasAltiVec
  
  
%endif