%ifndef SDL_EVENT_INC
%define SDL_EVENT_INC
  
  %include 'lib/SDL_keysym.inc'
  %include 'lib/SDL_mouse.inc'
  %include 'lib/SDL_joystick.inc'
  
  ; SDL Event type ------------------------------------------------
  
       SDL_NOEVENT 		equ 0	;< Unused (do not remove) */
       SDL_ACTIVEEVENT 		equ 1	;< Application loses/gains visibility */
       SDL_KEYDOWN		equ 2	;< Keys pressed */
       SDL_KEYUP 		equ 3	;< Keys released */
       SDL_MOUSEMOTION		equ 4	;< Mouse moved */
       SDL_MOUSEBUTTONDOWN	equ 5	;< Mouse button pressed */
       SDL_MOUSEBUTTONUP	equ 6	;< Mouse button released */
       SDL_JOYAXISMOTION	equ 7	;< Joystick axis motion */
       SDL_JOYBALLMOTION	equ 8	;< Joystick trackball motion */
       SDL_JOYHATMOTION		equ 9	;< Joystick hat position change */
       SDL_JOYBUTTONDOWN	equ 10	;< Joystick button pressed */
       SDL_JOYBUTTONUP		equ 11	;< Joystick button released */
       SDL_QUIT			equ 12	;< User-requested quit */
       SDL_SYSWMEVENT		equ 13	;< System specific event */
       SDL_EVENT_RESERVEDA	equ 14	;< Reserved for future use.. */
       SDL_EVENT_RESERVEDB	equ 15	;< Reserved for future use.. */
       SDL_VIDEORESIZE		equ 16	;< User resized video mode */
       SDL_VIDEOEXPOSE		equ 17	;< Screen needs to be redrawn */
       SDL_EVENT_RESERVED2	equ 18	;< Reserved for future use.. */
       SDL_EVENT_RESERVED3	equ 19	;< Reserved for future use.. */
       SDL_EVENT_RESERVED4	equ 20	;< Reserved for future use.. */
       SDL_EVENT_RESERVED5	equ 21	;< Reserved for future use.. */
       SDL_EVENT_RESERVED6	equ 22	;< Reserved for future use.. */
       SDL_EVENT_RESERVED7	equ 23	;< Reserved for future use.. */
       SDL_USEREVENT		equ 24
       
  ;------------------------FUNCTIONS-------------------------------
  
  extern SDL_PumpEvents
  extern SDL_PeepEvents
  extern SDL_PollEvent
  extern SDL_WaitEvent
  extern SDL_PushEvent
  
  extern SDL_SetEventFilter
  extern SDL_GetEventFilter
  extern SDL_EventState
  
  ;SDL_active.h
  extern SDL_GetAppState
  
  ;-----------------------EVENTS STRUCTS------------------------------
  struc SDL_Event
    .type: resb 1
    .active: resb SDL_ActiveEvent_size
    .key: resb SDL_KeyboardEvent_size
    .motion: resb SDL_MouseMotionEvent_size
    .button: resb SDL_MouseButtonEvent_size
    .jaxis: resb SDL_JoyAxisEvent_size
    .jball: resb SDL_JoyBallEvent_size
    .jhat: resb SDL_JoyHatEvent_size
    .jbutton: resb SDL_JoyButtonEvent_size
    .resize: resb SDL_ResizeEvent_size
    .expose: resb SDL_ExposeEvent_size
    .quit: resb SDL_QuitEvent_size
    .user: resb SDL_UserEvent_size
    .sysqm: resb SDL_SysWMEvent_size
  endstruc
  
  struc SDL_ActiveEvent
    .type: resb 1
    .gain: resb 1
    .state: resb 1
  endstruc
  
  struc SDL_KeyboardEvent
    .type: resb 1
    .state: resb 1
    .which: resb 1
    .keysym: resb SDL_keysym_size
  endstruc
  
  struc SDL_keysym
    .scancode: resb 1
    .sym: resd 1
    .mod: resd 1
    .unused: resd 1
  endstruc
  
  struc SDL_MouseMotionEvent
    .type: resb 1
    .state: resb 1
    .x: resd 1
    .y: resd 1
    .xrel: resd 1
    .yrel: resd 1
  endstruc
  
  struc SDL_MouseButtonEvent
    .type: resb 1
    .button: resb 1
    .state: resb 1
    .x: resw 1
    .y: resw 1
  endstruc
  
  struc SDL_JoyAxisEvent
    .type: resb 1
    .which: resb 1
    .axis: resb 1
    .value resw 1
  endstruc

  struc SDL_JoyBallEvent
    .type: resb 1
    .which: resb 1
    .ball: resb 1
    .xrel: resb 1
    .yrel: resb 1
  endstruc
  
  struc SDL_JoyHatEvent
    .type: resb 1
    .which: resb 1
    .hat: resb 1
    .value: resb 1
  endstruc

  struc SDL_JoyButtonEvent
    .type: resb 1
    .which: resb 1
    .button: resb 1
    .state: resb 1
  endstruc
  
  struc SDL_ResizeEvent
    .type: resb 1
    .w: resd 1
    .h: resd 1
  endstruc
  
  struc SDL_ExposeEvent
    .type: resb 1
  endstruc

  struc SDL_QuitEvent
    .type: resb 1
  endstruc
  
  struc SDL_UserEvent
    .type: resb 1
    .code: resd 1
    .data1: resd 1
    .data2: resd 1
  endstruc

  struc SDL_SysWMEvent
    .type: resb 1
    .msg: resd 1
  endstruc

%endif