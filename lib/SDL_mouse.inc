%ifndef SDL_MOUSE_INC
%define SDL_MOUSE_INC

  ; SDL_mouse include

  extern SDL_GetMouseState
  extern SDL_GetRelativeMouseState
  extern SDL_WarpMouse
  extern SDL_CreateCursor
  extern SDL_GetCursor
  extern SDL_FreeCursor
  extern SDL_ShowCursor

%endif