;%ifndef SDL_VIDEO_INC
;%define SDL_VIDEO_INC

; SDL video

  SDL_SWSURFACE equ 0x00000000
  SDL_HWSURFACE equ 0x00000001
  SDL_ASYNCBLIT equ 0x00000004


; Available for SDL_SetVideoMode() 
 SDL_ANYFORMAT	equ	0x10000000	;< Allow any video depth/pixel-format 
 SDL_HWPALETTE	equ	0x20000000	;< Surface has exclusive palette 
 SDL_DOUBLEBUF	equ	0x40000000	;< Set up double-buffered video mode 
 SDL_FULLSCREEN	equ	0x80000000	;< Surface is a full screen display 
 SDL_OPENGL	equ	0x00000002      ;< Create an OpenGL rendering context 
 SDL_OPENGLBLIT	equ	0x0000000A	;< Create an OpenGL rendering context and use it for blitting 
 SDL_RESIZABLE	equ	0x00000010	;< This video mode may be resized 
 SDL_NOFRAME	equ	0x00000020	;< No window caption or edge frame 

 ;Functions
 
 extern SDL_VideoInit
 extern SDL_VideoQuit
 
 extern SDL_VideoDriverName
 extern SDL_GetVideoSurface
 extern SDL_GetVideoInfo
 extern SDL_VideoModeOK
 extern SDL_ListModes
 
 extern SDL_SetVideoMode
 extern SDL_UpdateRects
 extern SDL_UpdateRect
 
 extern SDL_SetGamma
 extern SDL_SetGammaRamp
 extern SDL_GetGammaRamp
 extern SDL_SetColors
 
 extern SDL_SetPallete
 extern SDL_MapRGB
 extern SDL_MapRGBA
 
 extern SDL_GetRGB
 extern SDL_GetRGBA
 
 extern SDL_CreateRGBSurface
 extern SDL_CReateRGBSurfaceFrom
 extern SDL_FreeSurface
 
 extern SDL_LockSurface
 extern SDL_UnlockSurface
 
 extern SDL_LoadBMP_RW
 extern SDL_SaveBMP_RW
 
 string _rb, "rb"
 string _wb, "wb"
 
 %macro SDL_LoadBMP 1
  call SDL_RWFromFile, %1, _rb
  SDL_Error_NULL
  call SDL_LoadBMP_RW, eax, dword 1
  SDL_Error_NULL
 %endmacro
 
 %macro SDL_SaveBMP 1
  call SDL_RWFromFile, %1, _rb
  SDL_Error_NULL
  call SDL_SaveBMP_RW, eax, dword 1
  SDL_Error_NULL
 %endmacro 
 
 extern SDL_SetColorKey
 extern SDL_SetAlpha
 extern SDL_SetClipRect
 extern SDL_GetClipRect
 extern SDL_ConvertSurface
 
 ;theese are more internal functions
 extern SDL_UpperBlit
 extern SDL_LowerBlit
 ;--------------------------
 
 extern SDL_FillRect
 extern SDL_DisplayFormat
 extern SDL_DisplayFormatAlpha
 
 extern SDL_CreateYUVOverlay
 extern SDL_LockYUVOverlay
 extern SDL_UnlockYUVOverlay
 extern SDL_DisplayYUVOverlay
 extern SDL_FreeYUVOverlay
 
 extern SDL_GL_LoadLibrary
 extern SDL_GL_GetProcAddress
 extern SDL_GL_SetAttribute
 extern SDL_GL_GetAttribute
 extern SDL_GL_SwapBuffers
 extern SDL_GL_UpdateRects
 extern SDL_GL_Lock
 extern SDL_GL_Unlock
 
 extern SDL_WM_SetCaption
 extern SDL_WM_GetCaption
 extern SDL_WM_SetIcon
 extern SDL_WM_IconifyWindow
 extern SDL_WM_ToggleFullScreen
 extern SDL_WM_GrabInput
 extern SDL_SoftStretch
 
 extern SDL_Flip
 
  ;-----------------------VIDEO STRUCTS------------------------------
  struc SDL_Rect
    .x: resw 1 ;sint16
    .y: resw 1 ;sint16
    .w: resw 1 ;sint16
    .h: resw 1 ;sint16
  endstruc
  
  struc SDL_Color
    .r: resb 1 ;uint8
    .g: resb 1 ;uint8
    .b: resb 1	;uint8
    .unused: resb 1 ;uint8
  endstruc
  
  struc SDL_Pallete
    .ncolors: resd 1 ; int
    .colors: resd 1 ; SDL_Color*
  endstruc
  
  struc SDL_PixelFormat
    .pallete: resd 1 ; SDL_Pallete*
    .BitsPerPixel: resb 1 ;uint8
    .BytesPerPixel: resb 1 ;uint8
    .Rloss: resb 1 ;uint8
    .Gloss: resb 1 ;uint8
    .Bloss: resb 1 ;uint8
    .ALoss: resb 1 ;uint8
    .Rshift: resb 1 ;uint8
    .Gshift: resb 1 ;uint8
    .Bshift: resb 1 ;uint8
    .Ashift: resb 1 ;uint8
    .Rmask: resd 1 ;uint32
    .Gmask: resd 1 ;uint32
    .Bmask: resd 1 ;uint32
    .Amask: resd 1 ;uint32
    .colorkey: resd 1 ;uint32
    .alpha: resb 1 ;uint8
  endstruc
  
  
  struc SDL_Surface
    .flags: resd 1 ;uint32
    .format: resd 1 ;*SDL_PixelFormat
    .w: resd 1 ;int
    .h: resd 1 ;int
    .pitch: resw 1 ;uint16
    .pixels: resd 1 ;void*
    .offset: resd 1 ;int
    
    .private_hwdata: resd 1 ;struct private_hwdata*
    
    .clip_rect: resb SDL_Rect_size
    
    .unused1: resd 1 ;uint32
    .locked: resd 1 ;uint32
    
    .map: resd 1 ;struct SDL_BlitMap*
    
    .format_version: resd 1 ;unsigned int
    
    .refcount: resd 1 ;int
  endstruc
  
;%endif