;SDL headers file
;-------------------------------------------NOTES--------------------------------
;Every inc file should be written like this in 4 parts
;PARTS:
;	1. Includes of other libraries
;	2. Constants declaration
;	3. Funcs declaration
;	4. Strutcs (they are too big)


; This SDL incs are written by Zaraka (zarakaangel@gmail.com) for VUT FIT
; They will be released for public. If anyone will ever use this (you should not)
; and find any bugs, contact me

%ifndef SDL_INC
%define SDL_INC

;general library
%include 'lib/general.inc'

;C library
%include 'lib/libc.inc'

;SDL headers
%include 'lib/SDL_event.inc'
%include 'lib/SDL_video.inc'
%include 'lib/SDL_error.inc'
%include 'lib/SDL_rwops.inc'
%include 'lib/SDL_timer.inc'
;%include 'lib/SDL_endian.inc'
%include 'lib/SDL_audio.inc'
%include 'lib/SDL_cpuinfo.inc'
%include 'lib/SDL_thread.inc'
%include 'lib/SDL_mutex.inc'



;SDL Init  -----------------------------------------------------
  SDL_INIT_TIMER equ 0x00000001 ; timer subsystem
  SDL_INIT_AUDIO equ 0x00000010 ; audio subsystem
  SDL_INIT_VIDEO equ 0x00000020 ; video subsystem
  SDL_INIT_JOYSTICK equ 0x00000200 ; joystick subsystem
  SDL_INIT_HAPTIC equ 0x00001000 ; haptic (force feedback) subsystem
  SDL_INIT_GAMECONTROLLER equ 0x00002000 ; controller subsystem
  SDL_INIT_EVENTS  equ 0x00004000 ; events subsystem
  
;THIS GARBAGE IS SDL2
; SDL Window  ------------------------------------------------
;  SDL_WINDOW_SHOWN equ 0x00000004
;  SDL_WINDOWPOS_CENTERED_MASK equ 0x2FFF0000

; SDL Renderer ------------------------------------------------
;  SDL_RENDERER_SOFTWARE equ 0x00000001
;  SDL_RENDERER_ACCELERATED equ 0x00000002
;  SDL_RENDERER_PRESENTVSYNC equ 0x00000004

  ;--------------------------FUNCTIONS EXTERNS-------------------------
  
  ;SDL2 functions are not used here!
  ;extern SDL_CreateWindow
  ;extern SDL_DestroyWindow
  ;extern SDL_CreateRenderer
  ;extern SDL_DestroyRenderer
  ;extern SDL_SetHintWithPriority
  ;extern SDL_SetRenderDrawColor
  ;extern SDL_RenderClear
  ;extern SDL_RenderPresent
  ;--------------------THIS SHOULD BE MOVED--------------------------
  
  ;SDL.h
  extern SDL_Init
  extern SDL_InitSubSystem
  extern SDL_Quit
  extern SDL_QuitSubSystem
  extern SDL_WasInit
  
  ;SDL_version.h
  extern SDL_Linked_Version
  
  ;SDL_loadso.h
  extern SDL_LoadObject
  extern SDL_LoadFunction
  extern SDL_UnloadObject
  
  ;some really good strings
  string _newline, 10
  string _color_red, 27, "[0;31m"
  string _color_green, 27, "[0;32m"
  string _color_yellow, 27, "[0;33m"
  string _color_blue, 27, "[0;34m"
  string _color_purple, 27, "[0;35m"
  string _color_cyan, 27, "[0;36m"
  string _color_normal, 27,"[0;37m"
  
  string _color_bold_red, 27, "[1;31m"
  string _color_bold_green, 27, "[1;32m"
  string _color_bold_yellow, 27, "[1;33m"
  string _color_bold_blue, 27, "[1;34m"
  string _color_bold_purple, 27, "[1;35m"
  string _color_bold_cyan, 27, "[1;36m"
  string _color_bold_normal, 27,"[1;37m"
  
  %macro SDL_Error 0
    cmp eax, 0
    je %%end
    call printf, _color_red
    add esp, 4
    call SDL_GetError
    call printf, eax
    add esp, 4

    call printf, _color_normal
    add esp, 4
    call printf, _newline
    add esp, 4
    call SDL_ClearError
  %%end:
  %endmacro
  
    %macro SDL_Error_NULL 0
    cmp eax, 0
    jne %%end
    call printf, _color_red
    add esp, 4
    call SDL_GetError
    call printf, eax
    add esp, 4

    call printf, _color_normal
    add esp, 4
    call printf, _newline
    add esp, 4
    call SDL_ClearError
  %%end:
  %endmacro
 
%endif