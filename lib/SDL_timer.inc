%ifndef SDL_TIMER_INC
%define SDL_TIMER_INC

  ;-------------------------TIMER------------------------------

  extern SDL_GetTicks
  extern SDL_Delay
  extern SDL_SetTimer

  extern SDL_AddTimer
  extern SDL_RemoveTimer

%endif