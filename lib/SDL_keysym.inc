%ifndef SDL_KEYSYM_INC
%define SDL_KEYSYM_INC

	SDLK_UNKNOWN		equ 0
	SDLK_FIRST		equ 0
	SDLK_BACKSPACE		equ 8
	SDLK_TAB		equ 9
	SDLK_CLEAR		equ 12
	SDLK_RETURN		equ 13
	SDLK_PAUSE		equ 19
	SDLK_ESCAPE		equ 27
	SDLK_SPACE		equ 32
	SDLK_EXCLAIM		equ 33
	SDLK_QUOTEDBL		equ 34
	SDLK_HASH		equ 35
	SDLK_DOLLAR		equ 36
	SDLK_AMPERSAND		equ 38
	SDLK_QUOTE		equ 39
	SDLK_LEFTPAREN		equ 40
	SDLK_RIGHTPAREN		equ 41
	SDLK_ASTERISK		equ 42
	SDLK_PLUS		equ 43
	SDLK_COMMA		equ 44
	SDLK_MINUS		equ 45
	SDLK_PERIOD		equ 46
	SDLK_SLASH		equ 47
	SDLK_0			equ 48
	SDLK_1			equ 49
	SDLK_2			equ 50
	SDLK_3			equ 51
	SDLK_4			equ 52
	SDLK_5			equ 53
	SDLK_6			equ 54
	SDLK_7			equ 55
	SDLK_8			equ 56
	SDLK_9			equ 57
	SDLK_COLON		equ 58
	SDLK_SEMICOLON		equ 59
	SDLK_LESS		equ 60
	SDLK_EQUALS		equ 61
	SDLK_GREATER		equ 62
	SDLK_QUESTION		equ 63
	SDLK_AT			equ 64
	; Skip uppercase letters
	 
	SDLK_LEFTBRACKET	equ 91
	SDLK_BACKSLASH		equ 92
	SDLK_RIGHTBRACKET	equ 93
	SDLK_CARET		equ 94
	SDLK_UNDERSCORE		equ 95
	SDLK_BACKQUOTE		equ 96
	SDLK_a			equ 97
	SDLK_b			equ 98
	SDLK_c			equ 99
	SDLK_d			equ 100
	SDLK_e			equ 101
	SDLK_f			equ 102
	SDLK_g			equ 103
	SDLK_h			equ 104
	SDLK_i			equ 105
	SDLK_j			equ 106
	SDLK_k			equ 107
	SDLK_l			equ 108
	SDLK_m			equ 109
	SDLK_n			equ 110
	SDLK_o			equ 111
	SDLK_p			equ 112
	SDLK_q			equ 113
	SDLK_r			equ 114
	SDLK_s			equ 115
	SDLK_t			equ 116
	SDLK_u			equ 117
	SDLK_v			equ 118
	SDLK_w			equ 119
	SDLK_x			equ 120
	SDLK_y			equ 121
	SDLK_z			equ 122
	SDLK_DELETE		equ 127
	; End of ASCII mapped keysyms 
        

	; @name International keyboard syms 
        
	SDLK_WORLD_0		equ 160		; 0xA0 
	SDLK_WORLD_1		equ 161
	SDLK_WORLD_2		equ 162
	SDLK_WORLD_3		equ 163
	SDLK_WORLD_4		equ 164
	SDLK_WORLD_5		equ 165
	SDLK_WORLD_6		equ 166
	SDLK_WORLD_7		equ 167
	SDLK_WORLD_8		equ 168
	SDLK_WORLD_9		equ 169
	SDLK_WORLD_10		equ 170
	SDLK_WORLD_11		equ 171
	SDLK_WORLD_12		equ 172
	SDLK_WORLD_13		equ 173
	SDLK_WORLD_14		equ 174
	SDLK_WORLD_15		equ 175
	SDLK_WORLD_16		equ 176
	SDLK_WORLD_17		equ 177
	SDLK_WORLD_18		equ 178
	SDLK_WORLD_19		equ 179
	SDLK_WORLD_20		equ 180
	SDLK_WORLD_21		equ 181
	SDLK_WORLD_22		equ 182
	SDLK_WORLD_23		equ 183
	SDLK_WORLD_24		equ 184
	SDLK_WORLD_25		equ 185
	SDLK_WORLD_26		equ 186
	SDLK_WORLD_27		equ 187
	SDLK_WORLD_28		equ 188
	SDLK_WORLD_29		equ 189
	SDLK_WORLD_30		equ 190
	SDLK_WORLD_31		equ 191
	SDLK_WORLD_32		equ 192
	SDLK_WORLD_33		equ 193
	SDLK_WORLD_34		equ 194
	SDLK_WORLD_35		equ 195
	SDLK_WORLD_36		equ 196
	SDLK_WORLD_37		equ 197
	SDLK_WORLD_38		equ 198
	SDLK_WORLD_39		equ 199
	SDLK_WORLD_40		equ 200
	SDLK_WORLD_41		equ 201
	SDLK_WORLD_42		equ 202
	SDLK_WORLD_43		equ 203
	SDLK_WORLD_44		equ 204
	SDLK_WORLD_45		equ 205
	SDLK_WORLD_46		equ 206
	SDLK_WORLD_47		equ 207
	SDLK_WORLD_48		equ 208
	SDLK_WORLD_49		equ 209
	SDLK_WORLD_50		equ 210
	SDLK_WORLD_51		equ 211
	SDLK_WORLD_52		equ 212
	SDLK_WORLD_53		equ 213
	SDLK_WORLD_54		equ 214
	SDLK_WORLD_55		equ 215
	SDLK_WORLD_56		equ 216
	SDLK_WORLD_57		equ 217
	SDLK_WORLD_58		equ 218
	SDLK_WORLD_59		equ 219
	SDLK_WORLD_60		equ 220
	SDLK_WORLD_61		equ 221
	SDLK_WORLD_62		equ 222
	SDLK_WORLD_63		equ 223
	SDLK_WORLD_64		equ 224
	SDLK_WORLD_65		equ 225
	SDLK_WORLD_66		equ 226
	SDLK_WORLD_67		equ 227
	SDLK_WORLD_68		equ 228
	SDLK_WORLD_69		equ 229
	SDLK_WORLD_70		equ 230
	SDLK_WORLD_71		equ 231
	SDLK_WORLD_72		equ 232
	SDLK_WORLD_73		equ 233
	SDLK_WORLD_74		equ 234
	SDLK_WORLD_75		equ 235
	SDLK_WORLD_76		equ 236
	SDLK_WORLD_77		equ 237
	SDLK_WORLD_78		equ 238
	SDLK_WORLD_79		equ 239
	SDLK_WORLD_80		equ 240
	SDLK_WORLD_81		equ 241
	SDLK_WORLD_82		equ 242
	SDLK_WORLD_83		equ 243
	SDLK_WORLD_84		equ 244
	SDLK_WORLD_85		equ 245
	SDLK_WORLD_86		equ 246
	SDLK_WORLD_87		equ 247
	SDLK_WORLD_88		equ 248
	SDLK_WORLD_89		equ 249
	SDLK_WORLD_90		equ 250
	SDLK_WORLD_91		equ 251
	SDLK_WORLD_92		equ 252
	SDLK_WORLD_93		equ 253
	SDLK_WORLD_94		equ 254
	SDLK_WORLD_95		equ 255		; 0xFF 
        

	; @name Numeric keypad 
        
	SDLK_KP0		equ 256
	SDLK_KP1		equ 257
	SDLK_KP2		equ 258
	SDLK_KP3		equ 259
	SDLK_KP4		equ 260
	SDLK_KP5		equ 261
	SDLK_KP6		equ 262
	SDLK_KP7		equ 263
	SDLK_KP8		equ 264
	SDLK_KP9		equ 265
	SDLK_KP_PERIOD		equ 266
	SDLK_KP_DIVIDE		equ 267
	SDLK_KP_MULTIPLY	equ 268
	SDLK_KP_MINUS		equ 269
	SDLK_KP_PLUS		equ 270
	SDLK_KP_ENTER		equ 271
	SDLK_KP_EQUALS		equ 272
        

	; @name Arrows + Home/End pad 
        
	SDLK_UP			equ 273
	SDLK_DOWN		equ 274
	SDLK_RIGHT		equ 275
	SDLK_LEFT		equ 276
	SDLK_INSERT		equ 277
	SDLK_HOME		equ 278
	SDLK_END		equ 279
	SDLK_PAGEUP		equ 280
	SDLK_PAGEDOWN		equ 281
        

	; @name Function keys 
        
	SDLK_F1			equ 282
	SDLK_F2			equ 283
	SDLK_F3			equ 284
	SDLK_F4			equ 285
	SDLK_F5			equ 286
	SDLK_F6			equ 287
	SDLK_F7			equ 288
	SDLK_F8			equ 289
	SDLK_F9			equ 290
	SDLK_F10		equ 291
	SDLK_F11		equ 292
	SDLK_F12		equ 293
	SDLK_F13		equ 294
	SDLK_F14		equ 295
	SDLK_F15		equ 296
        

	; @name Key state modifier keys 
        
	SDLK_NUMLOCK		equ 300
	SDLK_CAPSLOCK		equ 301
	SDLK_SCROLLOCK		equ 302
	SDLK_RSHIFT		equ 303
	SDLK_LSHIFT		equ 304
	SDLK_RCTRL		equ 305
	SDLK_LCTRL		equ 306
	SDLK_RALT		equ 307
	SDLK_LALT		equ 308
	SDLK_RMETA		equ 309
	SDLK_LMETA		equ 310
	SDLK_LSUPER		equ 311		;< Left "Windows" key 
	SDLK_RSUPER		equ 312		;< Right "Windows" key 
	SDLK_MODE		equ 313		;< "Alt Gr" key 
	SDLK_COMPOSE		equ 314		;< Multi-key compose key 
        

	; @name Miscellaneous function keys 
        
	SDLK_HELP		equ 315
	SDLK_PRINT		equ 316
	SDLK_SYSREQ		equ 317
	SDLK_BREAK		equ 318
	SDLK_MENU		equ 319
	SDLK_POWER		equ 320		;< Power Macintosh power key 
	SDLK_EURO		equ 321		;< Some european keyboards 
	SDLK_UNDO		equ 322		;< Atari keyboard has Undo 
       
%endif