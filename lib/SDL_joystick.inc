%ifndef SDL_VIDEO_INC
%define SDL_VIDEO_INC

  ; ---------------------------SDL_joystick.inc

  extern SDL_Numjoysticks
  extern SDL_JoystickName
  extern SDL_JoystickOpen
  extern SDL_JoystickOpened
  extern SDL_JoystickIndex
  extern SDL_JoystickNumAxes
  extern SDL_JoystickNumBalls
  extern SDL_JoystickNumHats
  extern SDL_JoystickNumButtons
  extern SDL_JoystickUpdate
  extern SDL_JoystickEventState
  extern SDL_JoystickGetAxis
  extern SDL_JoystickGetHat
  extern SDL_JoystickGetButton
  extern SDL_JoystickClose
  
%endif