%ifndef SDL_ERROR_INC
%define SDL_ERROR_INC  
  
  ;SDL_error.h
  extern SDL_SetError
  extern SDL_GetError
  extern SDL_ClearError
  
%endif